package object;
import java.io.File;

public class ObjectFile {
    private File inputFile;
    private File keyFile;

    public ObjectFile() {
    }

    public File getInputFile() {
        return inputFile;
    }

    public void setInputFile(File inputFile) {
        this.inputFile = inputFile;
    }

    public File getKeyFile() {
        return keyFile;
    }

    public void setKeyFile(File keyFile) {
        this.keyFile = keyFile;
    }
}
