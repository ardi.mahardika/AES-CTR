import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import util.StringUtil;
import org.apache.commons.lang3.ArrayUtils;

import object.ObjectFile;

class AES_CTR {
	static String startEncrypt(ObjectFile file) throws Exception{
		Path inputPath = Paths.get(file.getInputFile().getAbsolutePath());
		Path keyPath = Paths.get(file.getKeyFile().getAbsolutePath());
		
		String keyHex = Files.readAllLines(keyPath).get(0);
		byte[] keyBytes = StringUtil.hexStringToByteArray(keyHex);
		byte[] input = Files.readAllBytes(inputPath);
		
		return encrypt(file.getInputFile().getName(), input, keyBytes, inputPath);
	}
	
	static String startDecrypt(ObjectFile file) throws Exception{
		Path inputPath = Paths.get(file.getInputFile().getAbsolutePath());
		Path keyPath = Paths.get(file.getKeyFile().getAbsolutePath());
		
		String keyHex = Files.readAllLines(keyPath).get(0);
		byte[] keyBytes = StringUtil.hexStringToByteArray(keyHex);
		byte[] input = Files.readAllBytes(inputPath);
		
		return decrypt(input, keyBytes, inputPath);
	}
	
	private static String encrypt(String name, byte[] input, byte[] keyBytes, Path inputPath) throws Exception{
		// TODO Auto-generated method stub
		SecretKeySpec key = new SecretKeySpec(keyBytes, "AES");
		Cipher cipher = Cipher.getInstance("AES/CTR/PKCS5PADDING");
		
		String hexFileName = StringUtil.toHexString(name + ";");
		byte[] byteFileName = StringUtil.hexStringToByteArray(hexFileName);
		input = ArrayUtils.addAll(byteFileName, input);
		byte[] iv = StringUtil.generateIV();
		
		cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(iv));
		ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(input);
		CipherInputStream cipherInputStream = new CipherInputStream(byteArrayInputStream, cipher);
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		
		int cis;
		while((cis = cipherInputStream.read()) >= 0){
			byteArrayOutputStream.write(cis);
		} 
		
		byte[] cipherText = byteArrayOutputStream.toByteArray();
		Path cipherPath = Paths.get(inputPath.getParent() + "\\cipher.inc");
		Files.write(cipherPath, ArrayUtils.addAll(iv, cipherText));
		
		return cipherPath.toAbsolutePath().toString();
	}
	
	private static String decrypt(byte[] input, byte[] keyBytes, Path inputPath) throws Exception {
		// TODO Auto-generated method stub
		SecretKeySpec key = new SecretKeySpec(keyBytes, "AES");
		Cipher cipher = Cipher.getInstance("AES/CTR/PKCS5PADDING");
		
		ByteArrayOutputStream byteArrayOutputStream;
		
		byte[] ivByte = new byte[16];
		System.arraycopy(input, 0, ivByte, 0, 16);
		input = Arrays.copyOfRange(input, 16, input.length);
		
		cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(ivByte));
		byteArrayOutputStream = new ByteArrayOutputStream();
		
		CipherOutputStream cipherOutputStream = new CipherOutputStream(byteArrayOutputStream, cipher);
		cipherOutputStream.write(input);
		cipherOutputStream.close();
		
		byte[] decryptedByte = byteArrayOutputStream.toByteArray();
		int nameDelimiterPos =  StringUtil.findNameDelimiter(decryptedByte);
		decryptedByte = ArrayUtils.removeElement(decryptedByte, (byte)0x3B); // removing ";"
		
		byte[] byteFileName = new byte[nameDelimiterPos];
		System.arraycopy(decryptedByte, 0, byteFileName, 0, nameDelimiterPos);
		String fileName = StringUtil.fromHexString(StringUtil.toHexString(byteFileName));
		byte[] fileBytes = Arrays.copyOfRange(decryptedByte, nameDelimiterPos, decryptedByte.length);
		
		Path decryptedPath = Paths.get(inputPath.getParent() + "\\ " + fileName + "[Decrypted]");
		Files.write(decryptedPath, fileBytes);
		
		return decryptedPath.toAbsolutePath().toString();
	}
}
